using UnityEngine;
using HarmonyLib;
using RimWorld;
using Verse;
using System.Collections.Generic;
using System;
using Verse.Sound;

namespace ResearchScroll {
	[HarmonyPatch(typeof(Dialog_FormCaravan), "SelectApproximateBestFoodAndMedicine")]
	internal static class DisableAutoCalculateAndSelect
	{
		/**
		 * Simply block function execution on evry call regardLESS of value
		 */
		private static bool Prefix() {
			return false;
        }
    }

	[HarmonyPatch(typeof(Dialog_FormCaravan), "DrawAutoSelectCheckbox")]
	internal static class DrawAutoSelectCheckboxPatch
	{
		/**
		 * Simply block function execution on evry call regardLESS of value
		 */
		private static bool Prefix()
		{
			return false;
		}
	}
}
