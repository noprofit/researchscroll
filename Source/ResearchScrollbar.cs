using System.Reflection;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RimWorld;
using UnityEngine;
using Verse;

namespace ResearchScroll
{
    public class ResearchScroll : Mod
    {
        public ResearchScroll( ModContentPack content ) : base( content )
        {
            var harmony = new Harmony( "ResearchScroll" );
            harmony.PatchAll( Assembly.GetExecutingAssembly() );
            Log.Message("Allahu Akbar");
        }
    }
}