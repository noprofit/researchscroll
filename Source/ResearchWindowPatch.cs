using UnityEngine;
using HarmonyLib;
using RimWorld;
using Verse;
using System.Collections.Generic;
using System;
using Verse.Sound;

namespace ResearchScroll {
	[HarmonyPatch(typeof(TabDrawer),
	 nameof(TabDrawer.DrawTabs),
	 new[] { 
		 typeof(Rect), 
		 typeof(List<TabRecord>), 
		 typeof(float) } 
	 )]
	public class PatchAllTabs 
	{
		public static Vector2 scrollPos = Vector2.zero;
		
		private static bool Prefix(
			ref Rect baseRect, 
			List<TabRecord> tabs, 
			ref float maxTabWidth,
			ref TabRecord __result
		) {
			TabRecord tabRecord = null;
			TabRecord tabRecord2 = tabs.Find((TabRecord t) => t.Selected);
			float num = baseRect.width + (float)(tabs.Count - 1) * 10f;
			float tabWidth = num / (float)tabs.Count;
			float maxWidth = Text.CalcSize(tabs[0].label).x;
			bool drawScroll = false;
			float viewFx = 0f;

			foreach (TabRecord tab in tabs) {
				if (maxWidth < Text.CalcSize(tab.label).x) {
					maxWidth = Text.CalcSize(tab.label).x;
				}
			}

			if (tabWidth < maxWidth) {
				tabWidth = maxTabWidth;
				drawScroll = true;
			}

			if (tabWidth > maxTabWidth) {
				tabWidth = maxTabWidth;
			}

			if (drawScroll == true) {
				Rect view = new Rect(baseRect.xMin, -10f, baseRect.width, 38f);
				Rect position = new Rect(baseRect.xMin, -10f, baseRect.width, 54f);

				float total = tabWidth * tabs.Count - 40f;

				if (total > view.width) {
					view.width = total - 40f;
				}
				
				viewFx = 10f;
				Widgets.ScrollHorizontal(position, ref scrollPos, view, 20f);
				Widgets.BeginScrollView(position, ref scrollPos, view, true);
				GUI.BeginGroup(view);
			}
			else {
				Rect position = new Rect(baseRect);
				position.y -= 32f;
				position.height = 9999f;
				viewFx = 1f;
				GUI.BeginGroup(position);
			}
			Text.Anchor = TextAnchor.MiddleCenter;
			Text.Font = GameFont.Small;
			Func<TabRecord, Rect> func = (TabRecord tab) => new Rect((float)tabs.IndexOf(tab) * (tabWidth - 10f), viewFx, tabWidth, 32f);
			List<TabRecord> list = tabs.ListFullCopy<TabRecord>();
			if (tabRecord2 != null)
			{
				list.Remove(tabRecord2);
				list.Add(tabRecord2);
			}
			TabRecord tabRecord3 = null;
			List<TabRecord> list2 = list.ListFullCopy<TabRecord>();
			list2.Reverse();
			for (int i = 0; i < list2.Count; i++)
			{
				TabRecord tabRecord4 = list2[i];
				Rect rect = func(tabRecord4);
				if (tabRecord3 == null && Mouse.IsOver(rect))
				{
					tabRecord3 = tabRecord4;
				}
				MouseoverSounds.DoRegion(rect, SoundDefOf.Mouseover_Tab);
				if (Widgets.ButtonInvisible(rect, true))
				{
					tabRecord = tabRecord4;
				}
			}
			foreach (TabRecord tabRecord5 in list)
			{
				Rect rect2 = func(tabRecord5);
				tabRecord5.Draw(rect2);
			}
			Text.Anchor = TextAnchor.UpperLeft;
			if (drawScroll == true) {
				Widgets.EndScrollView();
			}
			GUI.EndGroup();
			if (tabRecord != null && tabRecord != tabRecord2)
			{
				SoundDefOf.RowTabSelect.PlayOneShotOnCamera(null);
				if (tabRecord.clickedAction != null)
				{
					tabRecord.clickedAction();
				}
			}
			__result = tabRecord;
			return false;
		}
	}
}
